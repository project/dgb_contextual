// $Id$
(function ($) {

Drupal.DGBcontextualLinks = Drupal.DGBcontextualLinks || {};

/**
 * Attach outline behavior for entry regions associated with contextual links.
 */
Drupal.behaviors.DGBcontextualLinks = function (context) {
 var $wrapper = $(this);
 var $region = $('.dgb-contextual-links-region');
 var $links = $wrapper.find('ul.dgb-contextual-links');
 var $trigger = $('.dgb-contextual-links-trigger').click(
  function () {
   $links.stop(true, true).slideToggle(100);
   $(this).parent().toggleClass('dgb-contextual-links-active');
   return false;
  }
 );
 // Attach hover behavior to trigger wrapper and ul.dgb-contextual-links.
 $('.dgb-contextual-links-wrapper').hover(
  function () { $(this).parent().addClass('dgb-contextual-links-region-active'); },
  function () { $(this).parent().removeClass('dgb-contextual-links-region-active'); }
 );
 // Hide the contextual links when user clicks a link or rolls out of the .dgb-contextual-links-region.
 $region.bind('mouseleave click', Drupal.DGBcontextualLinks.mouseleave);
 // Prepend the trigger.
 $wrapper.prepend($trigger);
}

/**
 * Disables outline for the entry region contextual links are associated with.
 */
Drupal.DGBcontextualLinks.mouseleave = function () {
 $(this)
  .find('.dgb-contextual-links-active').removeClass('dgb-contextual-links-active')
  .find('ul.dgb-contextual-links').hide();
};

})(jQuery);