// $Id$

Drupal Guestbook Contextual (DGBC) provides contextual links to perform actions
related to a guestbook entry.

DGBC is a backport for the DGB module of the same D7 functionality.

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.
- The Drupal Guestbook (DGB) module.

Installation
--------------------------------------------------------------------------------
1. Copy the DGBC module folder to your module directory and then enable on the
   admin modules page.
2. Configure the current theme CSS for the needed contextual position. Read more
   in the documentation below.

Administration
--------------------------------------------------------------------------------
No administration and permission settings available.

Documentation
--------------------------------------------------------------------------------
- Contextual links position

  To place the contextual links in the entries further to the left or right
  please put and modify the example code in your current theme CSS file.

  Example 1

  No user picture are used. Place the contextual links on the right side.

  div.dgb-contextual-links-wrapper {
    right: 2px;
  }

  Example 2

  User pictures are used. This is the default module configuration.

  div.dgb-contextual-links-wrapper {
    right: 35px;
  }

  Define the 35px value depending on the size of the user picture.